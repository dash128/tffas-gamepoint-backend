package com.gamepoint.app;

import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    // http://localhost:8080/swagger-ui.html
    public static final String name ="Dash";
    public static final String url ="https://www.facebook.com/jordy.rojasaliaga";
    public static final String email ="rojas71421@gmail.com";

    public static final Contact DEFAULT_CONTACT = new Contact(name, url, email);

    public static final String title ="GamePointApp Api Documentation";
    public static final String description ="GamePointApp Api Documentation";
    public static final String version ="1.0";
    public static final String termsOfServiceUrl ="PREMIUM";
    public static final Contact contact = DEFAULT_CONTACT;
    public static final String license ="Apache 2.0";
    public static final String licenseUrl ="http://www.apache.org/licenses/LICENSE-2.0";
    
    public static final ApiInfo DEFAULT_API_INFO = new ApiInfo(title, description, version, termsOfServiceUrl, contact, license, licenseUrl, new ArrayList<VendorExtension>());

    @Bean
    public Docket api(){
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(DEFAULT_API_INFO);
    }
}