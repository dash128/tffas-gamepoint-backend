package com.gamepoint.app.service;

import com.gamepoint.app.model.entities.JuegoPorLanCenter;

import java.util.List;

public interface JuegoPorLanCenterService {

    void registrar(List<JuegoPorLanCenter> listaJuegosPorLanCenter);

    List<JuegoPorLanCenter> buscarPorLanCenter(Integer lanCenterId);

}