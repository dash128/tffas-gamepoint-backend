package com.gamepoint.app.service.impl;

import java.util.List;
import java.util.Optional;

import com.gamepoint.app.model.entities.Torneo;
import com.gamepoint.app.model.repository.TorneoRepository;
import com.gamepoint.app.service.TorneoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TorneoServiceImpl implements TorneoService {

    @Autowired
    private TorneoRepository torneoRepository;

    @Override
    public Torneo registrar(Torneo t) {
        return torneoRepository.save(t);
    }

    @Override
    public Torneo modificar(Torneo t) {
        return torneoRepository.save(t);
    }

    @Override
    public void eliminar(int id) {
        torneoRepository.deleteById(id);
    }

    @Override
    public Optional<Torneo> listarPorId(int id) {
        return torneoRepository.findById(id);
    }

    @Override
    public List<Torneo> listar() {
        return torneoRepository.findAll();
    }

    
}