package com.gamepoint.app.service.impl;

import java.util.List;
import java.util.Optional;

import com.gamepoint.app.model.entities.Equipo;
import com.gamepoint.app.model.repository.EquipoRepository;
import com.gamepoint.app.service.EquipoService;

import org.springframework.stereotype.Service;

@Service
public class EquipoServiceImpl implements EquipoService {

    private EquipoRepository equipoRepository;

    @Override
    public Equipo registrar(Equipo t) {
        return equipoRepository.save(t);
    }

    @Override
    public Equipo modificar(Equipo t) {
        return equipoRepository.save(t);
    }

    @Override
    public void eliminar(int id) {
        equipoRepository.deleteById(id);
    }

    @Override
    public Optional<Equipo> listarPorId(int id) {
        return equipoRepository.findById(id);
    }

    @Override
    public List<Equipo> listar() {
        return equipoRepository.findAll();
    }

    
}