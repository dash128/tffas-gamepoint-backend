package com.gamepoint.app.service.impl;

import java.util.List;
import java.util.Optional;

import com.gamepoint.app.model.entities.LanCenter;
import com.gamepoint.app.model.repository.LanCenterRepository;
import com.gamepoint.app.service.LanCenterService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LanCenterServiceImpl implements LanCenterService {

    @Autowired
    private LanCenterRepository lanCenterRepository;

    @Override
    public LanCenter registrar(LanCenter t) {
        return lanCenterRepository.save(t);
    }

    @Override
    public LanCenter modificar(LanCenter t) {
        return lanCenterRepository.save(t);
    }

    @Override
    public void eliminar(int id) {
        lanCenterRepository.deleteById(id);
    }

    @Override
    public Optional<LanCenter> listarPorId(int id) {
        return lanCenterRepository.findById(id);
    }

    @Override
    public List<LanCenter> listar() {
        return lanCenterRepository.findAll();
    }

    
}