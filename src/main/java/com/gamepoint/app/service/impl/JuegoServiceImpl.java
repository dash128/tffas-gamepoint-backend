package com.gamepoint.app.service.impl;

import java.util.List;
import java.util.Optional;

import com.gamepoint.app.model.entities.Juego;
import com.gamepoint.app.model.repository.JuegoRepository;
import com.gamepoint.app.service.JuegoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JuegoServiceImpl implements JuegoService {

    @Autowired
    private JuegoRepository juegoRepository;

    @Override
    public Juego registrar(Juego t) {
        return juegoRepository.save(t);
    }

    @Override
    public Juego modificar(Juego t) {
        return juegoRepository.save(t);
    }

    @Override
    public void eliminar(int id) {
        juegoRepository.deleteById(id);
    }

    @Override
    public Optional<Juego> listarPorId(int id) {
        return juegoRepository.findById(id);
    }

    @Override
    public List<Juego> listar() {
        return juegoRepository.findAll();
    }


}