package com.gamepoint.app.service.impl;

import java.util.List;

import com.gamepoint.app.model.entities.JuegoPorLanCenter;
import com.gamepoint.app.model.repository.JuegoPorLanCenterRepository;
import com.gamepoint.app.service.JuegoPorLanCenterService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class JuegoPorLanCenterServiceImpl implements JuegoPorLanCenterService{

    @Autowired
    private JuegoPorLanCenterRepository juegoPorLanCenterRepository;

    @Transactional
    @Override
    public void registrar(List<JuegoPorLanCenter> listaJuegosPorLanCenter) {
        listaJuegosPorLanCenter.forEach(juegoLanCenter->
            juegoPorLanCenterRepository.miRegistrar(juegoLanCenter.getJuego().getId(), juegoLanCenter.getLanCenter().getId()));
    }

    @Override
    public List<JuegoPorLanCenter> buscarPorLanCenter(Integer lanCenterId) {
        
        return juegoPorLanCenterRepository.miBuscarPorLanCenter(lanCenterId);
    }

    

}