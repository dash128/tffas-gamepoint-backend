package com.gamepoint.app.service.impl;

import java.util.List;
import java.util.Optional;

import com.gamepoint.app.model.entities.Jugador;
import com.gamepoint.app.model.repository.JugadorRepository;
import com.gamepoint.app.service.JugadorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JugadorServiceImpl implements JugadorService {

    @Autowired
    private JugadorRepository jugadorRepository;

    @Override
    public Jugador registrar(Jugador t) {
        return jugadorRepository.save(t);
    }

    @Override
    public Jugador modificar(Jugador t) {
        return jugadorRepository.save(t);
    }

    @Override
    public void eliminar(int id) {
        jugadorRepository.deleteById(id);
    }

    @Override
    public Optional<Jugador> listarPorId(int id) {
        return jugadorRepository.findById(id);
    }

    @Override
    public List<Jugador> listar() {
        return jugadorRepository.findAll();
    }

    
}