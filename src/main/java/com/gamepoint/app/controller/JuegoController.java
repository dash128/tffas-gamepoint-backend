package com.gamepoint.app.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.gamepoint.app.exception.ModeloNotFoundException;
import com.gamepoint.app.model.entities.Juego;
import com.gamepoint.app.service.JuegoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/juegos")
@CrossOrigin(origins = "http://localhost:4200")
@Api(value = "Servicio REST para los juegos")
public class JuegoController {

    @Autowired
    private JuegoService juegoService;

    @ApiOperation("Retorna la lista de juegos existentes")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Juego>> listar(){
        List<Juego> juegos = new ArrayList<>();
        juegos = juegoService.listar();

        return new ResponseEntity<List<Juego>>(juegos, HttpStatus.OK);
    }
    
    @GetMapping(value = "/{id}")
    public ResponseEntity<Juego> listarPorId(@PathVariable("id") Integer id){
        Optional<Juego> juego = juegoService.listarPorId(id);
        if(!juego.isPresent()){
            throw new ModeloNotFoundException("ID: "+id);
        } 

        return new ResponseEntity<Juego>(juego.get(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Juego> registrar(@Valid @RequestBody Juego juego){
        Juego juegoNew = new Juego();
        juegoNew = juegoService.registrar(juego);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(juegoNew.getId()).toUri();
        
        return ResponseEntity.created(location).build();
    }

    @PutMapping
    public ResponseEntity<Juego> actualizar(@Valid @RequestBody Juego juego){
        juegoService.modificar(juego);

        return new ResponseEntity<Juego>(HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void eliminar(@PathVariable("id") Integer id){
        Optional<Juego> juego = juegoService.listarPorId(id);
        if(!juego.isPresent()){
            throw new ModeloNotFoundException("ID: "+id);
        }else{
            juegoService.eliminar(id);
        }
    }
}