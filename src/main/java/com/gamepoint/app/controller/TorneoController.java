package com.gamepoint.app.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.gamepoint.app.exception.ModeloNotFoundException;
import com.gamepoint.app.model.entities.Torneo;
import com.gamepoint.app.service.TorneoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/torneos")
@CrossOrigin(origins = "http://localhost:4200")
@Api(value = "Servicio REST para los Torneos")
public class TorneoController {

    @Autowired
    public TorneoService torneoService;

    @ApiOperation("Retorna la lista de torneos existentes")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Torneo>> listar(){
        List<Torneo> torneos = new ArrayList<>();
        torneos = torneoService.listar();

        return new ResponseEntity<List<Torneo>>(torneos, HttpStatus.OK);
    }
    
    @GetMapping(value = "/{id}")
    public ResponseEntity<Torneo> listarPorId(@PathVariable("id") Integer id){
        Optional<Torneo> torneo = torneoService.listarPorId(id);
        if(!torneo.isPresent()){
            throw new ModeloNotFoundException("ID: "+id);
        }

        return new ResponseEntity<Torneo>(torneo.get(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Torneo> registrar(@Valid @RequestBody Torneo torneo){
        Torneo torneoNew = new Torneo();
        torneoNew = torneoService.registrar(torneo);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(torneoNew.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping
    public ResponseEntity<Torneo> actualizar(@Valid @RequestBody Torneo torneo){
        torneoService.modificar(torneo);

        return new ResponseEntity<Torneo>(HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void eliminar(@PathVariable("id") Integer id){
        Optional<Torneo> torneo = torneoService.listarPorId(id);
        if(!torneo.isPresent()){
            throw new ModeloNotFoundException("ID: "+id);
        }else{
            torneoService.eliminar(id);
        }
        
    }
}