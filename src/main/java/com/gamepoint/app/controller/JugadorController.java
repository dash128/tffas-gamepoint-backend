package com.gamepoint.app.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.gamepoint.app.exception.ModeloNotFoundException;
import com.gamepoint.app.model.entities.Jugador;
import com.gamepoint.app.service.JugadorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/jugadores")
@CrossOrigin(origins = "http://localhost:4200")
@Api(value = "Servicio REST para los jugadores")
public class JugadorController {

    @Autowired
    private JugadorService jugadorService;
    
    @ApiOperation("Retorna la lista de jugadores existentes")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Jugador>> listar(){
        List<Jugador> jugadores = new ArrayList<>();
        jugadores = jugadorService.listar();

        return new ResponseEntity<List<Jugador>>(jugadores, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Jugador> listarPorId(@PathVariable("id") Integer id){
        Optional<Jugador> jugador = jugadorService.listarPorId(id);
        if(!jugador.isPresent()){
            throw new ModeloNotFoundException("ID: "+id);
        }

        return new ResponseEntity<Jugador>(jugador.get(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Jugador> registrar(@Valid @RequestBody Jugador jugador){
        Jugador jugadorNew = new Jugador();
        jugadorNew = jugadorService.registrar(jugador);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(jugadorNew.getId()).toUri(); 
    
        return ResponseEntity.created(location).build();
    }

    @PutMapping
    public ResponseEntity<Jugador> actualizar(@Valid @RequestBody Jugador jugador){
        jugadorService.modificar(jugador);

        return new ResponseEntity<Jugador>(HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void eliminar(@PathVariable("id") Integer id){
        Optional<Jugador> jugador = jugadorService.listarPorId(id);
        if(!jugador.isPresent()){
            throw new ModeloNotFoundException("ID: "+id);
        }else{
            jugadorService.eliminar(id);
        }
    }
}