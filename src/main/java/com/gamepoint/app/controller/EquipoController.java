package com.gamepoint.app.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.gamepoint.app.exception.ModeloNotFoundException;
import com.gamepoint.app.model.entities.Equipo;
import com.gamepoint.app.service.EquipoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/equipos")
@CrossOrigin(origins = "http://localhost:4200")
@Api(value = "Servicio REST para los equipos")
public class EquipoController {
    
    @Autowired
    private EquipoService equipoService;

    @ApiOperation("Retorna la lista de equipos")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Equipo>> listar(){
        List<Equipo> equipos = new ArrayList<>();
        equipos = equipoService.listar();

        return new ResponseEntity<List<Equipo>>(equipos, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Equipo> listarPorId(@PathVariable("id") Integer id){
        Optional<Equipo> equipo = equipoService.listarPorId(id);
        if(!equipo.isPresent()){
            throw new ModeloNotFoundException("ID: "+id);
        }

        return new ResponseEntity<Equipo>(equipo.get(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Equipo> registrar(@Valid @RequestBody Equipo equipo){
        Equipo equipoNew = new Equipo();
        equipoNew = equipoService.registrar(equipo);
        URI location =  ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(equipoNew.getId()).toUri();
        
        return ResponseEntity.created(location).build();
    }
    
    @PutMapping
    public ResponseEntity<Equipo> actualizar(@Valid @RequestBody Equipo equipo){
        equipoService.modificar(equipo);

        return new ResponseEntity<Equipo>(HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void eliminar(@PathVariable("id") Integer id){
        Optional<Equipo> equipo = equipoService.listarPorId(id);
        if(!equipo.isPresent()){
            throw new ModeloNotFoundException("ID: "+id);
        }else{
            equipoService.eliminar(id);
        }
    }
}