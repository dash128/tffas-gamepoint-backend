package com.gamepoint.app.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.gamepoint.app.exception.ModeloNotFoundException;
import com.gamepoint.app.model.entities.JuegoPorLanCenter;
import com.gamepoint.app.model.entities.LanCenter;
import com.gamepoint.app.service.JuegoPorLanCenterService;
import com.gamepoint.app.service.LanCenterService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/lancenters")
@CrossOrigin(origins = "http://localhost:4200")
@Api(value = "Servicio REST para los lan centers")
public class LanCenterController {

    @Autowired
    private LanCenterService lanCenterService;

    @Autowired
    private JuegoPorLanCenterService juegoPorLanCenterService;

    @ApiOperation("Retorna la lista de juegos existentes")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<LanCenter>> listar(){
        List<LanCenter> lancenters = new ArrayList<>();
        lancenters = lanCenterService.listar();

        return new ResponseEntity<List<LanCenter>>(lancenters, HttpStatus.OK);
    }
    
    @GetMapping(value = "/{id}")
    public ResponseEntity<LanCenter> listarPorId(@PathVariable("id") Integer id){
        Optional<LanCenter> lancenter = lanCenterService.listarPorId(id);
        if(!lancenter.isPresent()){
            throw new ModeloNotFoundException("ID: "+id);
        }

        return new ResponseEntity<LanCenter>(lancenter.get(), HttpStatus.OK);
    }

    @GetMapping(value = "/juegos/{id}")
    public ResponseEntity<List<JuegoPorLanCenter>> listarJuegoPorLanCenter(@PathVariable("id") Integer id){
        List<JuegoPorLanCenter> misJuegos = new ArrayList<>();
        misJuegos = juegoPorLanCenterService.buscarPorLanCenter(id);

        return new ResponseEntity<List<JuegoPorLanCenter>>(misJuegos, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<LanCenter> registrar(@Valid @RequestBody LanCenter lancenter){
        LanCenter lancenterNew = new LanCenter();
        lancenterNew = lanCenterService.registrar(lancenter);
        URI location =  ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(lancenterNew.getId()).toUri();
        
        return ResponseEntity.created(location).build();
    }

    @ApiOperation("Almacena los juegos")
    @PostMapping(path = "/juegos")
    public void registrarJuegos(@Valid @RequestBody List<JuegoPorLanCenter> listaJuegosPorLanCenter){
        
        juegoPorLanCenterService.registrar(listaJuegosPorLanCenter);
    }

    @PutMapping
    public ResponseEntity<LanCenter> actualizar(@Valid @RequestBody LanCenter lancenter){
        lanCenterService.modificar(lancenter);

        return new ResponseEntity<LanCenter>(HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void eliminar(@PathVariable("id") Integer id){
        Optional<LanCenter> lancenter = lanCenterService.listarPorId(id);
        if(!lancenter.isPresent()){
            throw new ModeloNotFoundException("ID: "+id);
        }else{
            lanCenterService.eliminar(id);
        }
    }


}