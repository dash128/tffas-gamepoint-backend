package com.gamepoint.app.model.repository;

import com.gamepoint.app.model.entities.Juego;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JuegoRepository extends JpaRepository<Juego, Integer>{
    
    
}