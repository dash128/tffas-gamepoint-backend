package com.gamepoint.app.model.repository;

import com.gamepoint.app.model.entities.Jugador;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JugadorRepository extends JpaRepository<Jugador, Integer> {

    
}