package com.gamepoint.app.model.repository;

import com.gamepoint.app.model.entities.LanCenter;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LanCenterRepository extends JpaRepository<LanCenter, Integer>{

    
}