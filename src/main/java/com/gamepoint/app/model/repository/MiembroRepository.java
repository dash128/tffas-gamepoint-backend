package com.gamepoint.app.model.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MiembroRepository {

    public static final String queryInsert="";
    
    @Query(value = queryInsert, nativeQuery = true )
    Integer miRegistrar();

    
}