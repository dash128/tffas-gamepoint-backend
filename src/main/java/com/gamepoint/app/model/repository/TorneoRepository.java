package com.gamepoint.app.model.repository;

import com.gamepoint.app.model.entities.Torneo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TorneoRepository extends JpaRepository<Torneo, Integer>{

    
}