package com.gamepoint.app.model.repository;

import com.gamepoint.app.model.entities.Equipo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EquipoRepository extends JpaRepository<Equipo, Integer>{

    
}