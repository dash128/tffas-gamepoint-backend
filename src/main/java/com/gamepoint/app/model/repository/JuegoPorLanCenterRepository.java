package com.gamepoint.app.model.repository;

import java.util.List;

import com.gamepoint.app.model.entities.JuegoPorLanCenter;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface JuegoPorLanCenterRepository extends JpaRepository<JuegoPorLanCenter, Integer>{

    public static final String queryInsert="INSERT INTO Juego_Lancenter(juego_id, lancenter_id) VALUES(:juegoId, :lanCenterId)";
    public static final String queryBuscarPorLanCenter="FROM Juego_Lancenter WHERE lancenter_id=:lanCenterId";

    @Modifying

    @Query(value = queryInsert, nativeQuery = true )
    Integer miRegistrar(@Param("juegoId") Integer juegoId, @Param("lanCenterId") Integer lanCenterId);
    

    @Query(value = queryInsert, nativeQuery = true)
    List<JuegoPorLanCenter> miBuscarPorLanCenter(@Param("lanCenterId") Integer lanCenterId);

}