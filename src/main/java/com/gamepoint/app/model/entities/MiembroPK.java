package com.gamepoint.app.model.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class MiembroPK implements Serializable{

    private static final long serialVersionUID = 1L;

    @ManyToOne
    @JoinColumn(name = "jugador_id", nullable = false)
    private Jugador Jugador;

    @ManyToOne
    @JoinColumn(name = "equipo_id", nullable = false)
    private Equipo Equipo;
}