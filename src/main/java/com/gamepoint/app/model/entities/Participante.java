package com.gamepoint.app.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "Informacion de los participantes a torneos")
@Entity
@IdClass(ParticipantePK.class)
@Table(name = "Participante")
public class Participante {
    
    @Id
    private Equipo Equipo;

    @Id
    private Torneo Torneo;

    @Column(name = "puesto", nullable = false)
    private Integer Puesto;

    @Column(name = "aceptado", nullable = false)
    private Boolean Aceptado;

    public Equipo getEquipo() {
        return Equipo;
    }

    public void setEquipo(Equipo equipo) {
        Equipo = equipo;
    }

    public Torneo getTorneo() {
        return Torneo;
    }

    public void setTorneo(Torneo torneo) {
        Torneo = torneo;
    }

    public Integer getPuesto() {
        return Puesto;
    }

    public void setPuesto(Integer puesto) {
        Puesto = puesto;
    }

    public Boolean getAceptado() {
        return Aceptado;
    }

    public void setAceptado(Boolean aceptado) {
        Aceptado = aceptado;
    }

    
}