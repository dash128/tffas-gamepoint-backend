package com.gamepoint.app.model.entities;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "Informacion de los equipos")
@Entity
@Table(name = "Equipo")
public class Equipo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;
    
    @Column(name = "nombre", nullable = false, length = 70)
    private String Nombre;

    @ManyToOne
    @JoinColumn(name = "juego_id", nullable = false)
    private Juego Juego;

    @Column(name = "fecha", nullable = false, length = 70)
    private LocalDate Fecha;

    @Column(name = "logo", nullable = false, length = 70)
    private String Logo;

    
    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public LocalDate getFecha() {
        return Fecha;
    }

    public void setFecha(LocalDate fecha) {
        Fecha = fecha;
    }

    public String getLogo() {
        return Logo;
    }

    public void setLogo(String logo) {
        Logo = logo;
    }

    public Juego getJuego() {
        return Juego;
    }

    public void setJuego(Juego juego) {
        Juego = juego;
    }
    
    
}