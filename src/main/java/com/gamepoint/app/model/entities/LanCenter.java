package com.gamepoint.app.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "Información de los Lan Centers")
@Entity
@Table(name="Lancenter")
public class LanCenter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;
    
    @Column(name = "nombre", nullable = false, length = 70)
    private String Nombre;

    @Column(name = "direccion", nullable = false, length = 100)
    private String Direccion;

    @Column(name = "distrito", nullable = false, length = 70)
    private String Distrito;

    @Column(name = "latitud", nullable = false, length = 70)
    private String Latitud;

    @Column(name = "longitud", nullable = false, length = 70)
    private String Longitud;

    @Column(name = "correo", nullable = false, length = 70)
    private String Correo;

    @Column(name = "contraseña", nullable = false, length = 70)
    private String Contraseña;


    public Integer getId() {
        return this.Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getNombre() {
        return this.Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getDireccion() {
        return this.Direccion;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

    public String getDistrito() {
        return this.Distrito;
    }

    public void setDistrito(String Distrito) {
        this.Distrito = Distrito;
    }

    public String getLatitud() {
        return this.Latitud;
    }

    public void setLatitud(String Latitud) {
        this.Latitud = Latitud;
    }

    public String getLongitud() {
        return this.Longitud;
    }

    public void setLongitud(String Longitud) {
        this.Longitud = Longitud;
    }

    public String getCorreo() {
        return this.Correo;
    }

    public void setCorreo(String Correo) {
        this.Correo = Correo;
    }

    public String getContraseña() {
        return this.Contraseña;
    }

    public void setContraseña(String Contraseña) {
        this.Contraseña = Contraseña;
    }

}