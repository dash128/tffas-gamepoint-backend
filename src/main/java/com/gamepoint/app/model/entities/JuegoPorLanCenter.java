package com.gamepoint.app.model.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(JuegoPorLanCenterPK.class)
@Table(name = "Juego_Lancenter")
public class JuegoPorLanCenter {

    @Id
    private Juego juego;
    
    @Id
    private LanCenter lanCenter;

    public Juego getJuego() {
        return juego;
    }

    public void setJuego(Juego juego) {
        this.juego = juego;
    }

    public LanCenter getLanCenter() {
        return lanCenter;
    }

    public void setLanCenter(LanCenter lanCenter) {
        this.lanCenter = lanCenter;
    }
    
    
}