package com.gamepoint.app.model.entities;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "Información de los Torneos")
@Entity
@Table(name="Torneo")
public class Torneo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    @Column(name = "nombre", nullable = false, length = 70)
    private String Nombre;

    @Column(name = "fecha", nullable = false, length = 70)
    private LocalDate Fecha;

    @ManyToOne
    @JoinColumn(name = "juego_id", nullable = false)
    private Juego Juego;

    @ManyToOne
    @JoinColumn(name = "lancenter_id", nullable = false)
    private LanCenter lanCenter;


    public Integer getId() {
        return this.Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public LocalDate getFecha() {
        return this.Fecha;
    }

    public void setFecha(LocalDate Fecha) {
        this.Fecha = Fecha;
    }

    public Juego getJuego() {
        return this.Juego;
    }

    public void setJuego(Juego Juego) {
        this.Juego = Juego;
    }

    public LanCenter getLanCenter() {
        return this.lanCenter;
    }

    public void setLanCenter(LanCenter lanCenter) {
        this.lanCenter = lanCenter;
    }


}