package com.gamepoint.app.model.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class JuegoPorLanCenterPK implements Serializable{

    private static final long serialVersionUID = 1L;

    @ManyToOne
    @JoinColumn(name = "juego_id", nullable = false)
    private Juego juego;
    
    @ManyToOne
    @JoinColumn(name = "lancenter_id", nullable = false)
    private LanCenter lanCenter;
}