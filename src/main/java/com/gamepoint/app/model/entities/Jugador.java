package com.gamepoint.app.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "Informacion sobre los jugadores")
@Entity
@Table(name = "Jugador")
public class Jugador {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    @Column(name = "nick", nullable = false, length = 70)
    private String Nick;

    @Column(name = "correo", nullable = false, length = 70)
    private String Correo;

    @Column(name = "telefono", nullable = false, length = 70)
    private String Telefono;

    @Column(name = "nombres", nullable = false, length = 70)
    private String Nombres;

    @Column(name = "apellidos", nullable = false, length = 70)
    private String Apellidos;

    @Column(name = "contraseña", nullable = false, length = 70)
    private String Contraseña;
    

    public Integer getId() {
        return this.Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getNick() {
        return this.Nick;
    }

    public void setNick(String Nick) {
        this.Nick = Nick;
    }

    public String getCorreo() {
        return this.Correo;
    }

    public void setCorreo(String Correo) {
        this.Correo = Correo;
    }

    public String getTelefono() {
        return this.Telefono;
    }

    public void setTelefono(String Telefono) {
        this.Telefono = Telefono;
    }

    public String getNombres() {
        return this.Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public String getApellidos() {
        return this.Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public String getContraseña() {
        return this.Contraseña;
    }

    public void setContraseña(String Contraseña) {
        this.Contraseña = Contraseña;
    }
    
}