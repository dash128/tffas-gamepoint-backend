package com.gamepoint.app.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "Informacion de los miebros")
@Entity
@IdClass(MiembroPK.class)
@Table(name = "Miembro")
public class Miembro {

    @Id
    private Jugador Jugador;

    @Id
    private Equipo Equipo;

    @Column(name = "capitan", nullable = false)
    private Boolean Capitan;

    public Jugador getJugador() {
        return Jugador;
    }

    public void setJugador(Jugador jugador) {
        Jugador = jugador;
    }

    public Equipo getEquipo() {
        return Equipo;
    }

    public void setEquipo(Equipo equipo) {
        Equipo = equipo;
    }

    public Boolean getCapitan() {
        return Capitan;
    }

    public void setCapitan(Boolean capitan) {
        Capitan = capitan;
    }

    
}